package Java_Fundamentals;

import java.util.Scanner;

public class Exercises_1_to_17 {

    public static void main(String[] args) {
exercise15();

    }

    private static void Exercise1() {
        //1. Write a Java program to print 'Hello' on screen and then print your name on a separate line. Go to the editor
        //Expected Output :
        //Hello
        //Olivier Laborde
        System.out.println("Hello\nOlivier Laborde");
    }

    private static void exercise2() {
        //2. Write a Java program to print the sum of two numbers. Go to the editor
        //Test Data:
        //74 + 36
        //Expected Output :
        //110
        int n = 74;
        int m = 36;
        System.out.println(n + m);
    }

    private static void exercise3() {
        //3. Write a Java program to divide two numbers and print on the screen. Go to the editor
        //Test Data :
        //50/3
        //Expected Output :
        //16
        int n = 50;
        int m = 3;
        System.out.println(n / m);
    }

    private static void exercise4() {
        //4. Write a Java program to print the result of the following operations. Go to the editor
        //Test Data:
        //a. -5 + 8 * 6
        //b. (55+9) % 9
        //c. 20 + -3*5 / 8
        //d. 5 + 15 / 3 * 2 - 8 % 3
        //Expected Output :
        //43
        //1
        //19
        //13
        int a = (-5 + 8 * 6);
        int b = ((55 + 9) % 9);
        int c = (20 + (-3 * 5 / 8));
        int d = 5 + 15 / 3 * 2 - 8 % 3;
        System.out.printf("%d\n%d\n%d\n%d\n", a, b, c, d);
    }

    private static void exercise5() {
        //5. Write a Java program that takes two numbers as input and display the product of two numbers. Go to the editor
        //Test Data:
        //Input first number: 25
        //Input second number: 5
        //Expected Output :
        //25 x 5 = 125
        Scanner Input = new Scanner(System.in);
        System.out.println("Please input number 1");
        int a = Input.nextInt();
        System.out.println("Please input number 2");
        int b = Input.nextInt();
        int c = a * b;
        System.out.printf("Product of %d*%d is %d", a, b, c);
    }

    private static void exercise6() {
        //6. Write a Java program to print the sum (addition), multiply, subtract, divide and remainder of two numbers. Go to the editor
        //Test Data:
        //Input first number: 125
        //Input second number: 24
        //Expected Output :
        //125 + 24 = 149
        //125 - 24 = 101
        //125 x 24 = 3000
        //125 / 24 = 5
        //125 mod 24 = 5
        Scanner Input = new Scanner(System.in);
        int a = 125;
        int b = 24;
        int result;
        String Operator;

        System.out.println("Please indicate Operator ( +, -, x, / or mod)");

        Operator = Input.next();
        switch (Operator) {
            case "+":
                result = a + b;
                System.out.println("125 + 24 = " + result);
                break;
            case "-":
                result = a - b;
                System.out.println("125 - 24 = " + result);
                break;
            case "x":
                result = a * b;
                System.out.println("124 x 24 =" + result);
                break;
            case "mod":
                result = a % b;
                System.out.println("124 MOD 24 = " + result);
                break;
            default:
                System.out.println("Operator Input Error !");
                break;
        }
    }

    private static void exercise7() {
        //7. Write a Java program that takes a number as input and prints its multiplication table upto 10. Go to the editor
        //Test Data:
        //Input a number: 8
        //Expected Output :
        //8 x 1 = 8
        //8 x 2 = 16
        //8 x 3 = 24
        //...
        //8 x 10 = 80
        Scanner Input = new Scanner(System.in);
        System.out.println("Please Input number");
        int num = Input.nextInt();
        int result;

        for (int i = 1; i <= 10; i++) {
            result = num * i;
            System.out.println(num + " x " + i + " = " + result);
        }
    }
    private static void exercise8(){
        //8. Write a Java program to display the following pattern. Go to the editor
        //Sample Pattern :
        //
        //   J    a   v     v  a
        //   J   a a   v   v  a a
        //J  J  aaaaa   V V  aaaaa
        // JJ  a     a   V  a     a

        System.out.println("   J    a   v     v  a ");
        System.out.println("   J   a a   v   v  a a");
        System.out.println("J  J  aaaaa   V V  aaaaa");
        System.out.println(" JJ  a     a   V  a     a");
    }
    private static void exercise9(){
        //9. Write a Java program to compute the specified expressions and print the output. Go to the editor
        //Test Data:
        //((25.5 * 3.5 - 3.5 * 3.5) / (40.5 - 4.5))
        //Expected Output
        //2.138888888888889
        System.out.println((25.5*3.5-3.5*3.5)/(40.5-4.5));
    }

    private static void exercise10(){
        //10. Write a Java program to compute a specified formula. Go to the editor
        //Specified Formula :
        //4.0 * (1 - (1.0/3) + (1.0/5) - (1.0/7) + (1.0/9) - (1.0/11))
        //Expected Output
        //2.9760461760461765
        double pi = 4.0 * (1 - (1.0/3) + (1.0/5) - (1.0/7) + (1.0/9) - (1.0/11));
        System.out.println(pi);
    }
    private static void exercise11(){
        //11. Write a Java program to print the area and perimeter of a circle. Go to the editor
        //Test Data:
        //Radius = 7.5
        //Expected Output
        //Perimeter is = 47.12388980384689
        //Area is = 176.71458676442586
        double radius = 7.5;
        double area;
        double perimeter;
        area = 2*Math.PI*radius;
        perimeter = Math.PI*radius*radius;
        System.out.println("Area : " + area + "\n" + "Perimeter : " + perimeter);
    }
    private static void exercise12(){
        //12. Write a Java program that takes three numbers as input to calculate and print the average of the numbers. Go to the editor
        //Click me to see the solution
        int a;
        int b;
        int c;
        int result;

        Scanner Input = new Scanner(System.in);

        System.out.println("Please input Number 1");
        a = Input.nextInt();
        System.out.println("Please input Number 2");
        b = Input.nextInt();
        System.out.println("Please input Number 3");
        c = Input.nextInt();
        result = ((a+b+c)/3);

        System.out.println("Average : " + result);
    }
    private static void exercise13(){
        //13. Write a Java program to print the area and perimeter of a rectangle. Go to the editor
        //Test Data:
        //Width = 5.5 Height = 8.5
        //
        //Expected Output
        //Area is 5.6 * 8.5 = 47.60
        //Perimeter is 2 * (5.6 + 8.5) = 28.20
        final double a = 5.5;
        final double b = 8.5;
        double area;
        double perimeter;
        area = a*b;
        perimeter = ((a+b)*2);

        System.out.println("Area : " + area + " cm2." + "\n" + "Perimeter : " + perimeter + "cm.");
    }
    private static void exercise14(){
        //14. Write a Java program to print an American flag on the screen. Go to the editor
        //Expected Output
        //
        //* * * * * * ==================================
        // * * * * *  ==================================
        //* * * * * * ==================================
        // * * * * *  ==================================
        //* * * * * * ==================================
        // * * * * *  ==================================
        //* * * * * * ==================================
        // * * * * *  ==================================
        //* * * * * * ==================================
        //==============================================
        //==============================================
        //==============================================
        //==============================================
        //==============================================
        //==============================================
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
    }
    private static void exercise15(){
        //15. Write a Java program to swap two variables.

       // int a, b , temp;
        //        a= 15;
        //        b = 27;
        //        System.out.println("Before swapping : a, b = "+ a +" , "+ b);
        //        temp = a;
        //        a = b;
        //        b = temp;
        //        System.out.println("After swapping : a, b = "+ a + ", "+ b);

        int a, b;
        a= 15;
        b= 27;
        System.out.println("Before swapping : a, b = "+ a +" , "+ b);
        a = a + b;
        b = a - b;
        a = a - b;
        System.out.println("After swapping : a, b = "+ a + ", "+ b);
    }
    private static void exercise16(){
        //16. Write a Java program to print a face. Go to the editor
        //Expected Output
        //
        // +"""""+
        //[| o o |]
        // |  ^  |
        // | '-' |
        // +-----+
        System.out.println(" +\"\"\"\"\"+ ");
        System.out.println("[| o o |]");
        System.out.println(" |  ^  | ");
        System.out.println(" | '-' | ");
        System.out.println(" +-----+ ");
    }
    private static void exercise17(){
       //17. Write a Java program to add two binary numbers. Go to the editor
        //Input Data:
        //Input first binary number: 10
        //Input second binary number: 11
        //Expected Output
        //
        //Sum of two binary numbers: 101

    }
}




