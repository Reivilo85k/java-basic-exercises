package Java_Fundamentals;

import java.util.Scanner;

import static java.lang.Math.*;

public class Exercises_32_to_55 {
    public static void main(String[] args) {
exercise55();
    }
    private static void exercise32(){
        //32. Write a Java program to compare two numbers. Go to the editor
        //Input Data:
        //Input first integer: 25
        //Input second integer: 39
        //Expected Output
        //
        //25 != 39
        //25 < 39
        //25 <= 39
        int first;
        int second;

        Scanner in = new Scanner(System.in);

        System.out.println("Please input first integer");
        first = in.nextInt();
        System.out.println("Please input second integer");
        second = in.nextInt();

        if (first == second)
            System.out.printf("%d == %d\n", first, second);
        if (first != second)
            System.out.printf("%d != %d\n", first, second);
        if (first < second)
            System.out.printf("%d < %d\n", first,second);
        if (first > second)
            System.out.printf("%d > %d\n", first, second);
        if (first <= second)
            System.out.printf("%d <= %d\n", first, second);
        if (first >= second)
            System.out.printf("%d >= %d\n", first, second);
    }
    private static void exercise33(){
        Scanner in = new Scanner(System.in);
        System.out.println("Please input number");
        long n = in.nextLong();
        System.out.println("The sum of the digits is : " + sumDigits(n));

    }

    private static int sumDigits(long n){
        int sum = 0;
        while (n!=0){
            sum += n%10;
            n/=10;
        }
        return sum;

    }
    private static void exercise34(){
        //34. Write a Java program to compute the area of a hexagon. Go to the editor
        //Area of a hexagon = (6 * s^2)/(4*tan(π/6))
        //where s is the length of a side
        //Input Data:
        //Input the length of a side of the hexagon: 6
        //Expected Output
        //
        //The area of the hexagon is: 93.53074360871938
        Scanner in = new Scanner(System.in);
        double s;
        System.out.println("Please input the length of the hexagon side :");
        s=in.nextInt();

        System.out.println("the area of the Hexagon is " + area(s) +" cm2.");
    }
    private static double area(double s) {
        double y = pow(s, 2);
        return ((6 * y) / (4 * tan(PI / 6)));
    }
    private static void exercise35(){
        //35. Write a Java program to compute the area of a polygon. Go to the editor
        //Area of a polygon = (n*s^2)/(4*tan(π/n))
        //where n is n-sided polygon and s is the length of a side
        //Input Data:
        //Input the number of sides on the polygon: 7
        //Input the length of one of the sides: 6
        //Expected Output
        //
        //The area is: 130.82084798405722
        Scanner in = new Scanner(System.in);
        int n;
        double s;
        System.out.println("Please input the number of sides of the polygon");
        n=in.nextInt();
        System.out.println("Please input the length of a side");
        s=in.nextDouble();

        System.out.println("The polygon area is : " + PolygonArea(s,n) +"cm2");

    }
    public static double PolygonArea(double s, int n){
        double s2 = pow(s,2);
        return ((n*s2)/(4*tan(PI/n)));
    }
    private static void exercise36(){
        //36. Write a Java program to compute the distance between two points on the surface of earth. Go to the editor
        //Distance between the two points [ (x1,y1) & (x2,y2)]
        //d = radius * arccos(sin(x1) * sin(x2) + cos(x1) * cos(x2) * cos(y1 - y2))
        //Radius of the earth r = 6371.01 Kilometers
        //Input Data:
        //Input the latitude of coordinate 1: 25
        //Input the longitude of coordinate 1: 35
        //Input the latitude of coordinate 2: 35.5
        //Input the longitude of coordinate 2: 25.5
        //Expected Output
        //
        //The distance between those points is: 1480.0848451069087 km
        double x1;
        double y1;
        double x2;
        double y2;
        Scanner in = new Scanner (System.in);
        System.out.println("Input the latitude of coordinate 1 in km : ");
        x1 = in.nextDouble();
        System.out.println("Input the longitude of coordinate 1 in km : ");
        y1 = in.nextDouble();
        System.out.println("Input the latitude of coordinate 2 in km : ");
        x2 = in.nextDouble();
        System.out.println("Input the longitude of coordinate 2 in km : ");
        y2 = in.nextDouble();

        System.out.println("The distance between the two points is : " + distancePoints(x1,y1,x2,y2) + "km");

    }
    public static double distancePoints(double x1, double y1, double x2,double y2) {
        x1 = toRadians(x1);
        y1 = toRadians(y1);
        x2 = toRadians(x2);
        y2 = toRadians(y2);
        final double radius = 6371.01;
        double d;
        d = radius * acos(sin(x1) * sin(x2) + cos(x1) * cos(x2) * cos(y1 - y2));

        return d;
    }
    private static void exercise37(){
        //37. Write a Java program to reverse a string. Go to the editor
        //Input Data:
        //Input a string: The quick brown fox
        //Expected Output
        //
        //Reverse string: xof nworb kciuq ehT
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input a string : ");
        char[] arrayOfChar = scanner.nextLine().toCharArray();
        System.out.print("Reverse String : ");
        for (int i = arrayOfChar.length - 1; i >= 0; i--)  {
            System.out.println(arrayOfChar[i]);
        }
        System.out.println("\n");
    }
    private static void exercise38(){
        //38. Write a Java program to count the letters, spaces, numbers and other characters of an input string. Go to the editor
        //Expected Output
        //
        //The string is :  Aa kiu, I swd skieo 236587. GH kiu: sieo?? 25.33
        //letter: 23
        //space: 9
        //number: 10
        //other: 6
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please input String :");
        char[] characters = scanner.nextLine().toCharArray();
        int sumLetters = 0;
        int sumSpaces = 0;
        int sumNumbers = 0;
        int sumOthers = 0;
        for(int i = 0; i < characters.length; i++){
            if(Character.isLetter(characters[i])){
                sumLetters ++;
            }else if(Character.isSpaceChar(characters[i])){
                sumSpaces ++;
            }else if (Character.isDigit(characters[i])){
                sumNumbers ++;
            }else sumOthers ++;
        }
        System.out.print("Letter(s): "+sumLetters+"\n"+"Space(s): "+sumSpaces+"\n"+"Number(s): "+sumNumbers+"\n"+"Others :"+sumOthers);
    }
    private static void exercise39(){
        //39. Write a Java program to create and display unique three-digit number using 1, 2, 3, 4. Also count how many three-digit numbers are there. Go to the editor
        //Expected Output
        //
        //123
        //124
        //...
        //
        //431
        //432
        //Total number of the three-digit-number is 24
        int amount = 0;
        for(int i = 1; i <= 4; i++){
            for(int j = 1; j <= 4; j++){
                for(int k = 1; k <= 4; k++){
                    if(k != i && k != j && i != j){
                        amount++;
                        System.out.println(i + "" + j + "" + k);
                    }
                }
            }
        }
        System.out.println("Total number of the three-digit-number is " + amount);
    }
    private static void exercise43(){
        //43. Write a Java program to print the following string in a specific format (see the output). Go to the editor
        //Sample Output
        //
        //Twinkle, twinkle, little star,
        //	How I wonder what you are!
        //		Up above the world so high,
        //		Like a diamond in the sky.
        //Twinkle, twinkle, little star,
        //	How I wonder what you are
        System.out.println("\nTwinkle, twinkle, little star, \n\tHow I wonder what you are! \n\t\tUp above the world so high, \n\t\tLike a diamond in the sky. \nTwinkle, twinkle, little star, \n\tHow I wonder what you are!\n\n");
    }
    private static void exercise44(){
        //44. Write a Java program that accepts an integer (n) and computes the value of n+nn+nnn. Go to the editor
        //Sample Output:
        //
        //Input number: 5
        //5 + 55  + 555
        Scanner input = new Scanner(System.in);
        int n;
        System.out.println("Input number");
        n = input.nextInt();
        System.out.printf("%d + %d%d + %d%d%d\n",n,n,n,n,n,n);
    }
    private static void exercise48() {
        //48. Write a Java program to print the odd numbers from 1 to 99. Prints one number per line. Go to the editor
        //Sample Output:
        //
        //1
        //3
        //5
        //7
        //9
        //11
        //....
        //
        //91
        //93
        //95
        //97
        //99
        for (int i = 0; i <= 99; i++) {
            if (i % 2 != 0) {
                System.out.println(i + "\n");
            }
        }
    }
    private static void exercise49(){
        //49. Write a Java program to accept a number and check the number is even or not. Prints 1 if the number is even or 0 if the number is odd. Go to the editor
        //Sample Output:
        //
        //Input a number: 20
        //1
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        if (a % 2 == 0){
            System.out.println("1");
        } else {
            System.out.println("0");
        }
    }
    private static void exercise50(){
        //50. Write a Java program to print numbers between 1 to 100 which are divisible by 3, 5 and by both. Go to the editor
        //Sample Output:
        //
        //Divided by 3:
        //3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57
        //, 60, 63, 66, 69, 72, 75, 78, 81, 84, 87, 90, 93, 96, 99,
        //
        //Divided by 5:
        //5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90,
        //95,
        //
        //Divided by 3 & 5:
        //15, 30, 45, 60, 75, 90,

        System.out.println("\nDivided by 3: ");
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0) {
                System.out.print(i + ", ");
            }
        }
        System.out.println("\n\nDivided by 5: ");
        for (int i = 1; i <= 100; i++) {
            if (i % 5 == 0) {
                System.out.print(i + ", ");
            }

        }

        System.out.println("\n\nDivided by 3 and 5 : ");
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.print(i + ", ");
            }
        }
        System.out.println("\n");
    }
    private static void exercise51(){
        //51. Write a Java program to convert a string to an integer in Java. Go to the editor
        //Sample Output:
        //
        //Input a number(string): 25
        //The integer value is: 25

        Scanner input = new Scanner(System.in);
        System.out.println("Input number");
        String x = input.next();
        int result = Integer.parseInt(x);
        System.out.printf("The integer value is: %d\n",result);
    }
    private static void exercise52(){
        //52. Write a Java program to calculate the sum of two integers and return true if the sum is equal to a third integer. Go to the editor
        //Sample Output:
        //
        //Input the first number : 5
        //Input the second number: 10
        //Input the third number : 15
        //The result is: true

        Scanner in = new Scanner(System.in);
        System.out.println("Please input first number");
        int first = in.nextInt();
        System.out.println("Please input second number");
        int second = in.nextInt();
        System.out.println("Please input the third number");
        int third = in.nextInt();

        System.out.println("the result is : " + sumInt(first,second,third));
        System.out.println("\n");
    }
    public static boolean sumInt(int first, int second, int third){
        if ((first+second == third)||(first+third == second)||(second+third==first)){
            return true;
        }else return false;

    }
    private static void exercise53(){
        //53. Write a Java program that accepts three integers from the user and return true if the second number is greater than first number and third number is greater than second number. If "abc" is true second number does not need to be greater than first number. Go to the editor
        //Sample Output:
        //
        //Input the first number : 5
        //Input the second number: 10
        //Input the third number : 15
        //The result is: true

        Scanner in = new Scanner(System.in);
        System.out.print("Input the first number : ");
        int x = in.nextInt();
        System.out.print("Input the second number: ");
        int y = in.nextInt();
        System.out.print("Input the third number : ");
        int z = in.nextInt();
        System.out.print("The result is: "+test(x, y, z,true));
        System.out.print("\n");
    }
    public static boolean test(int p, int q, int r, boolean xyz){
        if(xyz)
            return (r > q);
        return (q > p && r > q);
    }
    private static void exercise54(){
        //54. Write a Java program that accepts three integers from the user and return true if two or more of them (integers ) have the same rightmost digit. The integers are non-negative. Go to the editor
        //Sample Output:
        //
        //Input the first number : 5
        //Input the second number: 10
        //Input the third number : 15
        //The result is: true

        Scanner in = new Scanner(System.in);
        System.out.println("Please input first number");
        int first = in.nextInt();
        System.out.println("Please input second number");
        int second = in.nextInt();
        System.out.println("Please input the third number");
        int third = in.nextInt();

        System.out.println("the result is : " + sumInt2(first, second, third));
        System.out.println("\n");
    }
    public static boolean sumInt2(int x, int y, int z) {
        int a = x % 10;
        int b = y % 10;
        int c = z % 10;

        return (a == b) || (a == c) || (b == c);
    }
    private static void exercise55(){
        //55. Write a Java program to convert seconds to hour, minute and seconds. Go to the editor
        //Sample Output:
        //
        //Input seconds: 86399
        //23:59:59

        Scanner in = new Scanner(System.in);
        System.out.println("Please input the number of seconds :\n");
        int secondsInput = in.nextInt();
        int secondsRemain = secondsRemain(secondsInput);
        int minutes = minutesConverter(secondsInput);
        int minutesRemain = minutesRemain(secondsInput);
        int hours = hoursConverter(minutes);

        if (secondsInput < 60) {
            System.out.printf("There are %d seconds", secondsInput);
        } else if (minutes < 60) {
            System.out.printf("Result is %d minute(s) and %d seconds", minutes, secondsRemain(secondsInput));
        } else System.out.printf("Result is %d hour(s) %d minute(s) and %d seconds", hours, minutesRemain(secondsInput), secondsRemain(secondsInput));

        System.out.println("\n\n");
    }
    private static int secondsRemain(int secondsInput) {
        return secondsInput % 60;
    }
    private static int minutesConverter(int secondsInput) {
        return (secondsInput/60);
    }
    private static int minutesRemain(int secondsInput){
        return (secondsInput%60);
    }
    private static int hoursConverter(int minutes) {
        return (minutes / 60);
    }
}
