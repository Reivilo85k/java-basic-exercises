package Java_Fundamentals;

import java.util.Arrays;
import java.util.Scanner;

class Exercises_56_to_ {
    public static void main(String[] args) {
        Exercise56();

    }

    public static void Exercise56(){
        //56. Write a Java program to find the number of integers within the range of two specified numbers and that are divisible by another number. Go to the editor
        //For example x = 5, y=20 and p =3, find the number of integers within the range x..y and that are divisible by p i.e. { i :x ≤ i ≤ y, i mod p = 0 }
        //Sample Output:
        //5
        Scanner in = new Scanner(System.in);
        System.out.println("Please input x:");
        int x = in.nextInt();
        System.out.println("Please input Y:");
        int y = in.nextInt();
        System.out.println("Please input p:");
        int p = in.nextInt();

        System.out.println(Arrays.toString(divideResult(x, y, p)));

    }
    public static int[] divideResult(int x, int y, int p) {
        int[] result = new int[arrayLength(x,y,p)];
        int position = 0;

        for (int i = x; i <= y; i++) {
            if (checkDivide(i, p)) {
                result[position] = i;
                position++;
            }
        }return result;
    }
    public static int arrayLength(int x, int y, int p) {
        int arrayLength = 0;

        for (int i = x; i <= y; i++) {
            if (checkDivide(i, p)){
            arrayLength++;
            }
        }return arrayLength;
    }

    public static boolean checkDivide(int i, int p){
        return i%p == 0;
    }

    public static void Exercise57(){
        //57. Write a Java program to accepts an integer and count the factors of the number. Go to the editor
        //Sample Output:
        //
        //Input an integer: 25
        //3


    }

}
